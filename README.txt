
-- SUMMARY -- 

The ContactBlock module allows an administrator to define a contact form which displays in a block region.

-- REQUIREMENTS --

None.

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

Unlike many modules, ContactBlock does not have an "Configure" area accessable through the Modules listing page. Rather, you will find configuration settings in Structure -> Blocks in the row where you enable the ContactBlock block. In this area, you will see several options. 

First is the normal option for "Block Title" which will display just like a normal block title. Leave blank for no block title.

Second, you will see the "Recipient Email Address" field. This is where all input from the form will be sent when the user hits "Submit" on the form. You need to have access to this account so you can see the messages that are sent through the form, which will appear in your inbox.

Third, you must define the "Sender Email Address." This is a bit decieving, because this variable is NOT the email address of the user who fills out the form. Rather, it is a trivial email address which you will need to define here. It will appear on the "From" line of the emails you recieve from the form. This has been set up this way so that you can avoid having messages from the form delivered to your spam folder. Simply ensure that your "Sender Email Address" is in the contact book of the "Recipient Email Address" account. In fact, this field does not even need to be a real email address: As long as it looks like a valid email address, it will work without issues.

The input field width setting defines how wide the boxes of your form will be. This value is defined in number of characters the input box will contain, not in pixels. Note that this only affects the "Name" and "Email Address" field widths. The "Message" field is automatically determined. Later releases will do away with this nasty hack and have all fields work like the "Message" field - automatically generated widths. 

Finally, you can define a "Message From Description" which is simply text which displays below the "Message" box on your form. It's a great place to give brief instructions to your user.

-- CONTACT --

I am user "sheldonkreger" on Drupal.org. 
http://drupal.org/user/1500290

I am available for freelance Drupal projects, including module development.
http://sheldonkreger.com

